part of 'viewmodels.dart';

class HomeVM extends BaseVM {
  final APIService _apiService = locator<APIService>();
  final GlobalKey<ScaffoldState> scaffoldkey = new GlobalKey();

  int countOfPages = 4;
  bool _isError = false;
  bool get isError => _isError;
  set isError(bool value) {
    _isError = value;
    notifyListeners();
  }

  List<Product> _listProduct;
  List<Product> get listProduct => _listProduct;

  String _sort = 'asc';
  String get sort => _sort;
  setSort(String value) {
    _sort = value;
    notifyListeners();
  }

  String _valueCategory = '';
  String get valueCategory => _valueCategory;
  setValueCategory(String value) {
    _valueCategory = value;
    notifyListeners();
  }

  List<String> _listCategory = [];
  List<String> get listCategory => _listCategory;

  int _page = 1;
  int get page => _page;
  set page(int value) {
    _page = value;
    notifyListeners();
  }

  int _endPage;
  int get endPage => _endPage;
  set endPage(int value) {
    _endPage = value;
    notifyListeners();
  }

  Future getCategoryList(BuildContext context) async {
    List<String> _list = await _apiService.getCategoryList(
          emptyCallback: (message) => showSnackBar(context, text: message),
          errorCallback: (message) => showSnackBar(context, text: message),
        ) ??
        [];
    _listCategory = _list ?? [];
    notifyListeners();
    return;
  }

  Future getProductList(BuildContext context, String sort) async {
    List<Product> _list = await _apiService.getProductList(
          sort: _sort,
          category: _valueCategory,
          limit: '$countOfPages',
          emptyCallback: (message) => showSnackBar(context, text: message),
          errorCallback: (message) => showSnackBar(context, text: message),
        ) ??
        [];
    _listProduct = _list ?? [];
    if ((_list?.length ?? 0) < countOfPages) {
      endPage = 1;
    }
    notifyListeners();
    return;
  }

  Future onLoadNextProduct(BuildContext context) async {
    if (isError) {
      isError = false;
    }
    if (endPage == null) {
      page = page + 1;
    }
    int _offset = page * countOfPages - (countOfPages - 1);
    int _limit = page * countOfPages;

    print('start = $_offset');
    print('limit = $_limit');

    List<Product> _list = await _apiService.getProductList(
          sort: _sort,
          limit: '$_limit',
          category: _valueCategory,
          emptyCallback: (message) => showSnackBar(context, text: message),
          errorCallback: (message) => showSnackBar(context, text: message),
        ) ??
        [];

    if (_list != null) {
      _listProduct?.addAll(_list);
      final ids = _listProduct?.map((e) => e.id)?.toSet();
      _listProduct?.retainWhere((x) => ids.remove(x.id));

      if (_list.length == 0 || _list.length < countOfPages) {
        endPage = page;
      }
    }
    notifyListeners();
    return;
  }

  Future onRefreshList(BuildContext context) async {
    _listProduct = null;
    notifyListeners();
    getCategoryList(context);
    endPage = null;
    page = 1;
    _listProduct = await _apiService.getProductList(
          sort: _sort,
          limit: '$countOfPages',
          category: _valueCategory,
          emptyCallback: (message) => showSnackBar(context, text: message),
          errorCallback: (message) => showSnackBar(context, text: message),
        ) ??
        [];
    notifyListeners();
  }
}
