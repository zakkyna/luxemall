import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:luxemall/ui/widgets/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:luxemall/locator.dart';
import 'package:luxemall/services/services.dart';
import 'package:luxemall/models/models.dart';

part 'base_vm.dart';
part 'home_vm.dart';
part 'cart_vm.dart';
