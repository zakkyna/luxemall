part of 'viewmodels.dart';

class BaseVM extends ChangeNotifier {
  bool _busy = false;
  bool get busy => _busy;
  final NavigationService _navigationService = locator<NavigationService>();

  void setBusy(bool value) {
    _busy = value;
    notifyListeners();
  }

  void navigateTo(String route) {
    _navigationService.navigateTo(route);
    setBusy(false);
  }

  void navigateOff(String route) {
    _navigationService.navigateOff(route);
    setBusy(false);
  }

  Future to(Widget route) {
    return _navigationService.to(route);
  }

  void off(Widget route) {
    _navigationService.off(route);
  }

  void goBack([dynamic result]) {
    _navigationService.goBack(result);
    setBusy(false);
  }

  void showCustomDialog({
    BuildContext context,
    String title,
    Widget titleIcon,
    String description,
    String labelCancel,
    String labelSubmit,
    Function onSubmit,
    Function onClose,
    bool singleButton,
    Color colorSubmit,
  }) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomDialogBox(
          title: title,
          titleIcon: titleIcon,
          descriptions: description,
          labelCancel: labelCancel,
          labelSubmit: labelSubmit,
          colorSubmit: colorSubmit,
          onSubmit: onSubmit,
          singleButton: singleButton,
        );
      },
    ).then((value) {
      if (onClose != null) {
        onClose();
      }
    });
  }

  void showPopUp({
    @required BuildContext context,
    Widget content(BuildContext context),
  }) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: content(context),
        );
      },
    );
  }

  void showSnackBar(BuildContext context, {String text, Widget icon}) {
    Flushbar(
      message: text,
      icon: icon == null
          ? Icon(
              Icons.warning_rounded,
              size: 30.0,
              color: Colors.white,
            )
          : icon,
      duration: Duration(seconds: 3),
      margin: EdgeInsets.all(10),
      borderRadius: 8,
    )..show(context);
  }
}
