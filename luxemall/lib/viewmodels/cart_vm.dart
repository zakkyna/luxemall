part of 'viewmodels.dart';

class CartVM extends BaseVM {
  final APIService _apiService = locator<APIService>();
  final GlobalKey<ScaffoldState> scaffoldkey = new GlobalKey();

  List<CartProduct> _listCartProduct = [];
  List<CartProduct> get listCartProduct => _listCartProduct;

  List<Product> _listProduct = [];
  List<Product> get listProduct => _listProduct;

  int _cartId;
  int get cartId => _cartId;
  set cartId(int value) {
    _cartId = value;
    notifyListeners();
  }

  Future<bool> addToCartSingleItem(
      BuildContext context, CartProduct cart) async {
    List<CartProduct> _list = await _apiService.addToCart(
          userid: 5,
          cart: cart,
          emptyCallback: (message) => showSnackBar(context, text: message),
          errorCallback: (message) => showSnackBar(context, text: message),
        ) ??
        [];

    if (_list != null) {
      if (_listCartProduct.singleWhere(
              (it) => it.productId == _list.first.productId,
              orElse: () => null) ==
          null) {
        _listCartProduct.addAll(_list);
      } else {
        _listCartProduct
            .singleWhere((it) => it.productId == _list.first.productId,
                orElse: () => null)
            .quantity += _list.first.quantity;
      }
      await Future.forEach(_list, (item) async {
        Product _product =
            await _apiService.getProductFromId(productId: item.productId);
        _listProduct.add(_product);
        print('product :$listProduct');
      });

      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  setCart(int productId, int quantity) {
    _listCartProduct
        .singleWhere((it) => it.productId == productId, orElse: () => null)
        .quantity = quantity;
    notifyListeners();
  }

  removeItem(int productId) {
    _listCartProduct.removeWhere((it) => it.productId == productId);
    notifyListeners();
  }
}
