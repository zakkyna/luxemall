import 'package:get_it/get_it.dart';
import 'package:luxemall/services/services.dart';
import 'package:luxemall/viewmodels/viewmodels.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => BaseVM());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => APIService());
  locator.registerLazySingleton(() => HomeVM());
  locator.registerLazySingleton(() => CartVM());
}
