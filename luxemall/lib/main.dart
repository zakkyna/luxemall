import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:luxemall/shared/shared.dart';
import 'package:luxemall/ui/pages/pages.dart';
import 'package:provider/provider.dart';
import 'package:luxemall/locator.dart';
import 'package:luxemall/services/services.dart';
import 'package:luxemall/viewmodels/viewmodels.dart';

void main() async {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => locator<BaseVM>(),
        ),
        ChangeNotifierProvider(
          create: (_) => locator<HomeVM>(),
        ),
        ChangeNotifierProvider(
          create: (_) => locator<CartVM>(),
        ),
      ],
      child: MaterialApp(
        title: 'Luxemall',
        navigatorKey: locator<NavigationService>().navigatorKey,
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: HomePage(),
      ),
    );
  }
}
