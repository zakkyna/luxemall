part of 'services.dart';

class APIService {
  final String baseUrl = 'https://fakestoreapi.com';
  Client client = Client();

  Future<List<Product>> getProductList({
    @required String limit,
    @required String sort,
    @required String category,
    Function(String message) errorCallback,
    Function(String message) emptyCallback,
    Function(String message) successCallback,
  }) async {
    try {
      String ct = category.isEmpty ? '' : '/category/$category';
      final response = await client.get(
        '$baseUrl/products$ct?limit=$limit&sort=$sort',
      );
      print(response.body);
      final _response = json.decode(response.body);

      if (response.statusCode == 200) {
        List _data = _response;
        if (_data.isEmpty) {
          if (emptyCallback != null) {
            emptyCallback('Empty');
          }
          return [];
        } else {
          if (successCallback != null) {
            successCallback('Success');
          }
          List<Product> _list =
              List<Product>.from(_data.map((item) => Product.fromMap(item)));
          return _list;
        }
      } else {
        if (errorCallback != null) {
          errorCallback('Error');
        }
        return null;
      }
    } catch (e) {
      print(e);
      if (errorCallback != null) {
        errorCallback(e.toString());
      }
      return null;
    }
  }

  Future<List<String>> getCategoryList({
    Function(String message) errorCallback,
    Function(String message) emptyCallback,
    Function(String message) successCallback,
  }) async {
    try {
      final response = await client.get(
        '$baseUrl/products/categories',
      );
      print(response.body);
      final _response = json.decode(response.body);

      if (response.statusCode == 200) {
        List _data = _response;
        if (_data.isEmpty) {
          if (emptyCallback != null) {
            emptyCallback('Empty');
          }
          return [];
        } else {
          if (successCallback != null) {
            successCallback('Success');
          }
          List<String> _list = List<String>.from(_data.map((item) => item));
          return _list;
        }
      } else {
        if (errorCallback != null) {
          errorCallback('Error');
        }
        return null;
      }
    } catch (e) {
      print(e);
      if (errorCallback != null) {
        errorCallback(e.toString());
      }
      return null;
    }
  }

  Future<List<CartProduct>> addToCart({
    @required int userid,
    @required CartProduct cart,
    Function(String message) errorCallback,
    Function(String message) emptyCallback,
    Function(String message) successCallback,
  }) async {
    try {
      final response = await client.post(
        '$baseUrl/carts',
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          "userId": '$userid',
          "date": "2020-02-03",
          "products": [cart.toJson()],
        }),
      );
      print({
        "userId": '$userid',
        "date": "2020-02-03",
        "products": [cart.toJson()],
      });
      print(response.body);
      final _response = json.decode(response.body);

      if (response.statusCode == 200) {
        List _data = _response['products'];
        if (_data.isEmpty) {
          if (emptyCallback != null) {
            emptyCallback('Empty');
          }
          return [];
        } else {
          if (successCallback != null) {
            successCallback('Success');
          }
          List<CartProduct> _list = List<CartProduct>.from(
              _data.map((item) => CartProduct.fromMap(item)));
          return _list;
        }
      } else {
        if (errorCallback != null) {
          errorCallback('Error');
        }
        return null;
      }
    } catch (e) {
      print(e);
      if (errorCallback != null) {
        errorCallback(e.toString());
      }
      return null;
    }
  }

  Future<Product> getProductFromId({
    @required int productId,
    Function(String message) errorCallback,
    Function(String message) emptyCallback,
    Function(String message) successCallback,
  }) async {
    try {
      final response = await client.get(
        '$baseUrl/products/$productId',
      );
      print(response.body);
      final _response = json.decode(response.body);

      if (response.statusCode == 200) {
        Map _data = _response;
        if (_data.isEmpty) {
          if (emptyCallback != null) {
            emptyCallback('Empty');
          }
          return null;
        } else {
          if (successCallback != null) {
            successCallback('Success');
          }
          Product product = Product.fromMap(_data);
          return product;
        }
      } else {
        if (errorCallback != null) {
          errorCallback('Error');
        }
        return null;
      }
    } catch (e) {
      print(e);
      if (errorCallback != null) {
        errorCallback(e.toString());
      }
      return null;
    }
  }
}
