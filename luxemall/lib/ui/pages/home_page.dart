part of 'pages.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _homeProvider = Provider.of<HomeVM>(context);

    return StatefulWrapper(
      onInit: () async {
        _homeProvider.getCategoryList(context);
        _homeProvider.getProductList(context, _homeProvider.sort);
      },
      child: GeneralPage(
        scaffoldkey: _homeProvider.scaffoldkey,
        drawer: DrawerApp(),
        leftIcon: Icon(
          Icons.menu_open_rounded,
          color: Colors.white,
          size: defaultIconSize,
        ),
        onLeftIconTap: () {
          _homeProvider.scaffoldkey.currentState.openDrawer();
        },
        rightWidget: GestureDetector(
          onTap: () {
            _homeProvider.to(CartPage());
          },
          child: Icon(
            Icons.shopping_bag,
            color: Colors.white,
            size: defaultIconSize,
          ),
        ),
        title: 'Products',
        child: Stack(
          children: [
            PaginationListView(
              busy: _homeProvider.listProduct == null,
              isError: _homeProvider.isError,
              isEnd: _homeProvider.endPage != null,
              itemBuilder: (context, index) {
                Product product = _homeProvider.listProduct[index];
                return ProductItem(product);
              },
              itemCount: _homeProvider.listProduct?.length ?? 0,
              onRefresh: () => _homeProvider.onRefreshList(
                context,
              ),
              onLoad: (loaded) => _homeProvider
                  .onLoadNextProduct(
                    context,
                  )
                  .then((a) => loaded()),
            ),
            Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  margin: EdgeInsets.all(defaultMargin),
                  child: ElevatedButton(
                    onPressed: () {
                      _homeProvider.showPopUp(
                          context: context,
                          content: (ctx) => FiltersPopup(ctx));
                    },
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10)),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(mainColor),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(
                            color: mainColor,
                          ),
                        ),
                      ),
                    ),
                    child: Icon(
                      Icons.filter_list_rounded,
                      color: Colors.white,
                      size: defaultIconSize,
                    ),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
