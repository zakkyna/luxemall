part of 'pages.dart';

class CartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _cartProvider = Provider.of<CartVM>(context);
    return GeneralPage(
      drawer: DrawerApp(),
      leftIcon: Icon(
        Icons.arrow_back_ios,
        color: Colors.white,
        size: defaultIconSize,
      ),
      onLeftIconTap: () {
        _cartProvider.goBack();
      },
      rightWidget: GestureDetector(
        onTap: () {
          _cartProvider.off(HomePage());
        },
        child: Icon(
          Icons.home_rounded,
          color: Colors.white,
          size: defaultIconSize,
        ),
      ),
      title: 'Cart',
      child: Stack(
        children: [
          _cartProvider.listCartProduct.length == 0
              ? Center(
                  child: Text(
                    'No Cart Item',
                    style: blackFontStyle2,
                  ),
                )
              : ListView(
                  children: [
                    SizedBox(
                      height: 50,
                    ),
                    Column(
                      children: _cartProvider.listCartProduct?.map((item) {
                            Product _prod = _cartProvider.listProduct
                                .firstWhere((i) => i.id == item.productId);
                            return CartItem(item, _prod);
                          })?.toList() ??
                          [SizedBox()],
                    ),
                    SizedBox(
                      height: 100,
                    ),
                  ],
                ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.all(defaultMargin),
              child: ElevatedButton(
                onPressed: () {},
                style: ButtonStyle(
                  padding: MaterialStateProperty.all<EdgeInsets>(
                      EdgeInsets.symmetric(
                          horizontal: defaultMargin, vertical: 10)),
                  backgroundColor: MaterialStateProperty.all<Color>(mainColor),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                      side: BorderSide(
                        color: mainColor,
                      ),
                    ),
                  ),
                ),
                child: Text(
                  'Checkout Now',
                  style: whiteLabelButton,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
