import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:luxemall/shared/shared.dart';
import 'package:luxemall/ui/widgets/widgets.dart';
import 'package:luxemall/viewmodels/viewmodels.dart';
import 'package:luxemall/models/models.dart';

import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

part 'general_page.dart';
part 'home_page.dart';
part 'detail_page.dart';
part 'cart_page.dart';
