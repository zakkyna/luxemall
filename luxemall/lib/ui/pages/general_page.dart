part of 'pages.dart';

class HeaderCurvedContainer extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = mainColor;
    Path path = Path()
      ..relativeLineTo(0, 80)
      ..quadraticBezierTo(size.width / 2, 160.0, size.width, 80)
      ..relativeLineTo(0, -80)
      ..close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class GeneralPage extends StatelessWidget {
  final Widget child;
  final Widget leftIcon;
  final Widget rightWidget;
  final String title;
  final TextStyle titleStyle;
  final Function onLeftIconTap;
  final Widget drawer;
  final GlobalKey<ScaffoldState> scaffoldkey;
  GeneralPage({
    this.child,
    this.leftIcon,
    this.rightWidget,
    this.drawer,
    this.title = "Title",
    this.titleStyle,
    this.onLeftIconTap,
    this.scaffoldkey,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawerEnableOpenDragGesture: false,
      key: scaffoldkey,
      backgroundColor: mainColor,
      drawer: drawer,
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Stack(
          children: [
            SafeArea(
              child: Container(
                color: Colors.white,
              ),
            ),
            SafeArea(
              child: Column(
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  Expanded(
                    child: child,
                  ),
                ],
              ),
            ),
            CustomPaint(
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 120,
              ),
              painter: HeaderCurvedContainer(),
            ),
            SafeArea(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: defaultMargin,
                    ),
                    width: double.infinity,
                    height: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        onLeftIconTap == null
                            ? SizedBox()
                            : GestureDetector(
                                onTap: onLeftIconTap,
                                child: leftIcon == null
                                    ? Icon(
                                        Icons.arrow_back_ios,
                                        color: Colors.white,
                                        size: defaultIconSize,
                                      )
                                    : leftIcon,
                              ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: defaultMargin,
                              vertical: 10,
                            ),
                            child: Text(
                              'Luxemall',
                              textAlign: TextAlign.center,
                              style: titleStyle ?? whiteTitle1,
                            ),
                          ),
                        ),
                        rightWidget == null ? SizedBox() : rightWidget,
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: defaultMargin,
                    ),
                    child: Text(
                      title,
                      style: titleStyle ?? whiteTitle2,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
