part of 'pages.dart';

class DetailPage extends StatefulWidget {
  final Product product;
  DetailPage(this.product);
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  int _n = 1;
  void minus() {
    setState(() {
      if (_n != 1) _n--;
    });
  }

  void add() {
    setState(() {
      _n++;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _base = Provider.of<BaseVM>(context);
    final _cartProvider = Provider.of<CartVM>(context);
    return GeneralPage(
      title: 'Details',
      leftIcon: Icon(
        Icons.arrow_back_ios,
        color: Colors.white,
        size: defaultIconSize,
      ),
      onLeftIconTap: () {
        _base.goBack();
      },
      rightWidget: GestureDetector(
        onTap: () {
          _base.to(CartPage());
        },
        child: Icon(
          Icons.shopping_bag,
          color: Colors.white,
          size: defaultIconSize,
        ),
      ),
      child: Stack(
        children: [
          ListView(
            padding: EdgeInsets.all(defaultMargin),
            children: [
              SizedBox(
                height: 50,
              ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: defaultMargin * 3),
                child: FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: widget.product.image,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(top: 10, right: 15),
                child: Text(
                  '\$ ${widget.product.price}',
                  style: blackFontStyle1.copyWith(
                      color: mainColor, fontWeight: FontWeight.w700),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                widget.product.title,
                style: blackFontStyle1.copyWith(
                  height: 1.3,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                widget.product.description,
                style: greyFontStyle3.copyWith(
                  height: 1.3,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text(
                    'Category : ',
                    style: blackFontStyle3,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 2, horizontal: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      border: Border.all(color: greyColor, width: 0.5),
                    ),
                    child: Text(
                      widget.product.category,
                      maxLines: 1,
                      style: blackFontStyle3.copyWith(
                        color: greyColor,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 100,
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(30)),
                color: greyColor,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RawMaterialButton(
                        onPressed: minus,
                        child: Icon(
                          Icons.remove,
                          color: Colors.black,
                          size: 20,
                        ),
                        fillColor: Colors.white,
                        padding: EdgeInsets.all(5.0),
                        shape: CircleBorder(),
                      ),
                      Container(
                        child: Text(
                          '$_n',
                          style: whiteLabelButton.copyWith(
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      RawMaterialButton(
                        onPressed: add,
                        child: Icon(
                          Icons.add,
                          color: Colors.black,
                          size: 20,
                        ),
                        fillColor: Colors.white,
                        padding: EdgeInsets.all(5.0),
                        shape: CircleBorder(),
                      ),
                    ],
                  ),
                  ElevatedButton(
                    onPressed: _base.busy
                        ? null
                        : () async {
                            _base.setBusy(true);
                            CartProduct cart = CartProduct(
                              productId: widget.product.id,
                              quantity: _n,
                            );
                            bool _success = await _cartProvider
                                .addToCartSingleItem(context, cart);
                            if (_success) _base.to(CartPage());
                            _base.setBusy(false);
                          },
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.symmetric(
                              horizontal: defaultMargin, vertical: 8)),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(mainColor),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          side: BorderSide(
                            color: mainColor,
                          ),
                        ),
                      ),
                    ),
                    child: _base.busy
                        ? Container(
                            height: 30,
                            child: SpinKitSpinningCircle(
                              color: Colors.white,
                            ))
                        : Text(
                            'Add to cart',
                            style: whiteLabelButton1,
                          ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
