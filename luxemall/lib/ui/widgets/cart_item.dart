part of 'widgets.dart';

class CartItem extends StatefulWidget {
  final CartProduct cart;
  final Product product;
  CartItem(this.cart, this.product);
  @override
  _CartItemState createState() => _CartItemState(cart.quantity);
}

class _CartItemState extends State<CartItem> {
  int _n;
  _CartItemState(int i) {
    _n = i;
  }

  void minus() {
    setState(() {
      if (_n != 1) _n--;
    });
  }

  void add() {
    setState(() {
      _n++;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _cartProvider = Provider.of<CartVM>(context);
    return ContainerCard(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 2,
            child: Container(
              alignment: Alignment.center,
              height: 120,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      padding: EdgeInsets.only(right: 15),
                      child: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: widget.product.image,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10, right: 15),
                    child: Text(
                      '\$ ${widget.product.price * _n}',
                      style: blackFontStyle2.copyWith(
                          color: mainColor, fontWeight: FontWeight.w700),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.product.title,
                  style: blackFontStyle3.copyWith(height: 1.3),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: RawMaterialButton(
                        onPressed: () {
                          if (_n == 1)
                            _cartProvider.removeItem(widget.product.id);
                          minus();
                          _cartProvider.setCart(widget.product.id, _n);
                        },
                        child: Icon(
                          Icons.remove,
                          color: Colors.black,
                          size: 20,
                        ),
                        fillColor: Colors.white,
                        padding: EdgeInsets.all(5.0),
                        shape: CircleBorder(),
                      ),
                    ),
                    Text(
                      '$_n',
                      style:
                          blackFontStyle3.copyWith(fontWeight: FontWeight.bold),
                    ),
                    Flexible(
                      child: RawMaterialButton(
                        onPressed: () {
                          add();
                          _cartProvider.setCart(widget.product.id, _n);
                        },
                        child: Icon(
                          Icons.add,
                          color: Colors.black,
                          size: 20,
                        ),
                        fillColor: Colors.white,
                        padding: EdgeInsets.all(5.0),
                        shape: CircleBorder(),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
