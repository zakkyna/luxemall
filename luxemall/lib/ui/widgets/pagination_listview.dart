part of 'widgets.dart';

class PaginationListView extends StatefulWidget {
  final Future<void> Function() onRefresh;
  final Function(VoidCallback) onLoad;
  final bool busy;
  final bool isError;
  final Widget Function(BuildContext, int) itemBuilder;
  final int itemCount;
  final bool isEnd;
  PaginationListView({
    @required this.onRefresh,
    @required this.onLoad,
    @required this.busy,
    @required this.isError,
    @required this.itemBuilder,
    @required this.itemCount,
    @required this.isEnd,
  });
  @override
  _PaginationListViewState createState() => _PaginationListViewState();
}

class _PaginationListViewState extends State<PaginationListView> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: NotificationListener<ScrollNotification>(
            onNotification: (ScrollNotification scrollInfo) {
              if (!isLoading &&
                  scrollInfo.metrics.pixels ==
                      scrollInfo.metrics.maxScrollExtent &&
                  !widget.isEnd) {
                // start loading data
                setState(() {
                  isLoading = true;
                });
                widget.onLoad(() => setState(() {
                      isLoading = false;
                    }));
              }
              return true;
            },
            child: Platform.isIOS
                ? CustomScrollView(
                    slivers: [
                      CupertinoSliverRefreshControl(
                          onRefresh: widget.onRefresh),
                      widget.busy
                          ? Center(
                              child: SpinKitRotatingCircle(
                              size: 30,
                              color: mainColor,
                            ))
                          : SliverList(
                              delegate: SliverChildBuilderDelegate(
                                widget.itemBuilder,
                                childCount: widget.itemCount,
                              ),
                            ),
                    ],
                  )
                : RefreshIndicator(
                    onRefresh: widget.onRefresh,
                    child: widget.busy
                        ? Center(
                            child: SpinKitRotatingCircle(
                            size: 30,
                            color: mainColor,
                          ))
                        : ListView.builder(
                            padding: EdgeInsets.symmetric(vertical: 50),
                            itemBuilder: widget.itemBuilder,
                            itemCount: widget.itemCount,
                          ),
                  ),
          ),
        ),
        if (widget.isError)
          Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                setState(() {
                  isLoading = true;
                });
                widget.onLoad(() => setState(() {
                      isLoading = false;
                    }));
              },
              child: Container(
                width: double.infinity,
                alignment: Alignment.center,
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Icon(
                      Icons.refresh_rounded,
                      color: Colors.grey,
                      size: 27,
                    ),
                    Text(
                      'Terjadi Kesalahan',
                      textAlign: TextAlign.center,
                      style: greyFontStyle2,
                    ),
                  ],
                ),
              ),
            ),
          ),
        if (isLoading && !widget.isError)
          Container(
            height: 50.0,
            color: Colors.transparent,
            child: Center(
              child: SpinKitRotatingCircle(
                color: mainColor,
                size: defaultIconSize,
              ),
            ),
          ),
      ],
    );
  }
}
