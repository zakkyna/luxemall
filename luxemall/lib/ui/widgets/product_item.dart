part of 'widgets.dart';

class ProductItem extends StatelessWidget {
  final Product product;
  ProductItem(this.product);
  @override
  Widget build(BuildContext context) {
    final _base = Provider.of<BaseVM>(context, listen: false);
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          _base.to(DetailPage(product));
        },
        child: Container(
          padding: EdgeInsets.all(15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 2,
                child: Container(
                  alignment: Alignment.center,
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(
                        child: Container(
                          padding: EdgeInsets.only(right: 15),
                          child: FadeInImage.memoryNetwork(
                            placeholder: kTransparentImage,
                            image: product.image,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 15),
                        child: Text(
                          '\$ ${product.price}',
                          style: blackFontStyle2.copyWith(
                              color: mainColor, fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      product.title,
                      maxLines: 2,
                      style: blackFontStyle3.copyWith(height: 1.3),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      product.description,
                      maxLines: 3,
                      style: greyFontStyle4.copyWith(height: 1.3),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 2, horizontal: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(20),
                        ),
                        border: Border.all(color: greyColor, width: 0.5),
                      ),
                      child: Text(
                        product.category,
                        maxLines: 1,
                        style: blackFontStyle4.copyWith(
                          color: greyColor,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
