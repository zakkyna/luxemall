part of 'widgets.dart';

class FiltersPopup extends StatelessWidget {
  final BuildContext ctx;
  FiltersPopup(this.ctx);
  @override
  Widget build(BuildContext context) {
    final _homeProvider = Provider.of<HomeVM>(context);
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
        color: Colors.white,
      ),
      width: double.infinity,
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.all(defaultMargin),
        children: [
          Text(
            'Sort by',
            style: blackFontStyle3,
            textAlign: TextAlign.left,
          ),
          Row(
            children: [
              Radio(
                value: 'asc',
                groupValue: _homeProvider.sort,
                onChanged: _homeProvider.setSort,
              ),
              Text(
                'Ascending',
                style: blackFontStyle4,
              )
            ],
          ),
          Row(
            children: [
              Radio(
                value: 'desc',
                groupValue: _homeProvider.sort,
                onChanged: _homeProvider.setSort,
              ),
              Text(
                'Descending',
                style: blackFontStyle4,
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Filter category',
            style: blackFontStyle3,
            textAlign: TextAlign.left,
          ),
          Row(
            children: [
              Radio(
                value: '',
                groupValue: _homeProvider.valueCategory,
                onChanged: _homeProvider.setValueCategory,
              ),
              Text(
                'All',
                style: blackFontStyle4,
              )
            ],
          ),
          Column(
            children: _homeProvider.listCategory
                    .map(
                      (item) => Row(
                        children: [
                          Radio(
                            value: item,
                            groupValue: _homeProvider.valueCategory,
                            onChanged: _homeProvider.setValueCategory,
                          ),
                          Text(
                            item,
                            style: blackFontStyle4,
                          )
                        ],
                      ),
                    )
                    .toList() ??
                [SizedBox()],
          ),
          SizedBox(
            height: 10,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ElevatedButton(
              onPressed: () {
                _homeProvider.onRefreshList(ctx);
                _homeProvider.goBack();
              },
              style: ButtonStyle(
                padding: MaterialStateProperty.all<EdgeInsets>(
                    EdgeInsets.symmetric(horizontal: 35, vertical: 5)),
                backgroundColor: MaterialStateProperty.all<Color>(mainColor),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(
                      color: mainColor,
                    ),
                  ),
                ),
              ),
              child: Text(
                'Apply',
                style: whiteLabelButton1,
              ),
            ),
          )
        ],
      ),
    );
  }
}
