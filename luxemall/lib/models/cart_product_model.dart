part of 'models.dart';

class CartProduct {
  int productId;
  int quantity;

  CartProduct({
    this.productId,
    this.quantity,
  });

  CartProduct.fromMap(Map snapshot)
      : productId = snapshot['productId'],
        quantity = snapshot['quantity'];

  toJson() {
    return {
      "productId": productId,
      "quantity": quantity,
    };
  }
}
