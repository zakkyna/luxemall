part of 'models.dart';

class Product {
  int id;
  String title;
  double price;
  String description;
  String category;
  String image;

  Product({
    this.id,
    this.title,
    this.price,
    this.description,
    this.category,
    this.image,
  });

  Product.fromMap(Map snapshot)
      : id = snapshot['id'],
        title = snapshot['title'],
        price = double.parse(snapshot['price'].toString()),
        description = snapshot['description'],
        category = snapshot['category'],
        image = snapshot['image'];
}
