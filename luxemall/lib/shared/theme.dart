part of 'shared.dart';

Color mainColor = "C2912E".toColor();
Color greyColor = "585858".toColor();

const double defaultMargin = 20;
const double defaultIconSize = 27;

TextStyle whiteTitle1 = GoogleFonts.dancingScript()
    .copyWith(color: Colors.white, fontSize: 24, fontWeight: FontWeight.w700);

TextStyle whiteTitle2 = GoogleFonts.mandali()
    .copyWith(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500);

TextStyle blackTitle =
    GoogleFonts.mandali().copyWith(fontSize: 20, fontWeight: FontWeight.w600);

TextStyle whiteSubTitle = GoogleFonts.mandali()
    .copyWith(color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal);

TextStyle blackFontStyle1 =
    GoogleFonts.mandali().copyWith(fontSize: 22, fontWeight: FontWeight.w500);

TextStyle blackFontStyle2 =
    GoogleFonts.mandali().copyWith(fontSize: 18, fontWeight: FontWeight.w500);

TextStyle blackFontStyle3 =
    GoogleFonts.mandali().copyWith(fontSize: 16, fontWeight: FontWeight.w500);

TextStyle blackFontStyle4 =
    GoogleFonts.mandali().copyWith(fontSize: 14, fontWeight: FontWeight.w500);

TextStyle greyFontStyle1 = GoogleFonts.mandali()
    .copyWith(color: greyColor, fontSize: 22, fontWeight: FontWeight.normal);

TextStyle greyFontStyle2 = GoogleFonts.mandali()
    .copyWith(color: greyColor, fontSize: 18, fontWeight: FontWeight.normal);

TextStyle greyFontStyle3 = GoogleFonts.mandali()
    .copyWith(color: greyColor, fontSize: 16, fontWeight: FontWeight.normal);

TextStyle greyFontStyle4 = GoogleFonts.mandali()
    .copyWith(color: greyColor, fontSize: 14, fontWeight: FontWeight.normal);

TextStyle whiteLabelButton1 = GoogleFonts.mandali()
    .copyWith(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500);

TextStyle greyLabelButton = GoogleFonts.mandali()
    .copyWith(color: greyColor, fontSize: 13, fontWeight: FontWeight.normal);

TextStyle whiteLabelButton = GoogleFonts.mandali()
    .copyWith(color: Colors.white, fontSize: 18, fontWeight: FontWeight.normal);
